<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/21/17
 * Time: 14:52
 */

namespace Inside\Core\Models;


class Order extends BaseModel
{
    const PENDING = 1;
    const PROCESSING = 2;
    const SHIPPED = 4;
    const COMPLETED = 6;
    const CANCEL = 8;
    const RETURNED = 10;
    const RE_PROCESSING = 12;
    const DELIVERED = 14;
    const PACKED = 16;
    const PICKING = 18;
    const PARTIAL_DELIVERY = 20;

    const ORDER_TYPE_NORMAL = 1;
    const ORDER_TYPE_COMPOUND = 2;

    protected $fillable = [
        'id',
        'code',
        'customer_id',
        'city_id',
        'city_name',
        'district_id',
        'district_name',
        'ward_id',
        'ward_name',
        'address',
        'user_id',
        'shipper_id',
        'shipping_from_date',
        'shipping_to_date',
        'shipping_company_id',
        'note',
        'priority',
        'total_item',
        'sub_total',
        'discount',
        'total',
        'voucher_code',
        'shipping_fee',
        'payment_amount',
        'payment_method',
        'status',
        'printed_date',
        'completed_date',
        'is_check',
        'store_id',
        'options',
        'billing_address_id',
        'shipping_address_id',
        'created_at',
        'updated_at',
        'mysql_id',
        'type', // Order type: 1: normal, 2 compound
        'parent_id', // Order id of parent
        'is_preorder', // O: none, 1: pre-order
        'items', // Item in order
        'pickup_store_id', // Store with proccess order
        'expected_delivery_time',
    ];

    public function setCodeAttribute($value){
        $this->attributes['code'] = (int)$value;
    }

    public function setCustomerIdAttribute($value){
        $this->attributes['customer_id'] = (int)$value;
    }
    public function setStatusAttribute($value){
        $this->attributes['status'] = (int)$value;
    }
    public function setWardIdAttribute($value){
        $this->attributes['ward_id'] = (int)$value;
    }

    public function setDistrictIdAttribute($value){
        $this->attributes['district_id'] = (int)$value;
    }
    public function setCityIdAttribute($value){
        $this->attributes['city_id'] = (int)$value;
    }
    public function setUserIdAttribute($value){
        $this->attributes['user_id'] = (int)$value;
    }

    public function setShipperIdAttribute($value){
        $this->attributes['shipper_id'] = (int)$value;
    }

    public function setShippingFromDateAttribute($value){
        $this->attributes['shipping_from_date'] = (int)$value;
    }

    public function setShippingToDateAttribute($value){
        $this->attributes['shipping_to_date'] = (int)$value;
    }

    public function setShippingCompanyIdAttribute($value){
        $this->attributes['shipping_company_id'] = (int)$value;
    }
    public function setPriorityAttribute($value){
        $this->attributes['priority'] = (int)$value;
    }
    public function setTotalItemAttribute($value){
        $this->attributes['total_item'] = (int)$value;
    }
    public function setSubTotalAttribute($value){
        $this->attributes['sub_total'] = (int)$value;
    }
    public function setDiscountAttribute($value){
        $this->attributes['discount'] = (int)$value;
    }
    public function setTotalAttribute($value){
        $this->attributes['total'] = (int)$value;
    }
    public function setShippingFeeAttribute($value){
        $this->attributes['shipping_fee'] = (int)$value;
    }
    public function setPaymentAmountAttribute($value){
        $this->attributes['payment_amount'] = (int)$value;
    }
    public function setPaymentMethodAttribute($value){
        $this->attributes['payment_method'] = (int)$value;
    }
    public function setPrintedDateAttribute($value){
        $this->attributes['printed_date'] = (int)$value;
    }
    public function setCompletedDateAttribute($value){
        $this->attributes['completed_date'] = (int)$value;
    }
    public function setIsCheckAttribute($value){
        $this->attributes['is_check'] = (int)$value;
    }

    public function setMysqlIdAttribute($value){
        $this->attributes['mysql_id'] = (int)$value;
    }

    public function setStoreIdAttribute($value){
        $this->attributes['store_id'] = (int)$value;
    }

    public function setOptionsAttribute($value){
        if(!$value){
            $this->attributes['options'] = [];
        }else{
            if(is_array($value)){
                $this->attributes['options'] = $value;
            }else{
                $this->attributes['options'] = json_decode($value, true);
            }

        }
    }

    public function setTypeAttribute($value){
        $this->attributes['type'] = (int)$value;
    }
    public function setParentIdAttribute($value){
        $this->attributes['parent_id'] = (int)$value;
    }
    public function setIsPreorderAttribute($value){
        $this->attributes['is_preorder'] = (int)$value;
    }
    public function setItemsAttribute($value){
        if($value){
            if(is_array($value)){
                $this->attributes['items'] = $value;
            }else{
                $this->attributes['items'] = json_decode($value, true);
            }
        }else{
            $this->attributes['items'] = [];
        }
    }
    public function setPickupStoreIdAttribute($value){
        $this->attributes['pickup_store_id'] = (int)$value;
    }
    public function setExpectedDeliveryTimeAttribute($value){
        $this->attributes['expected_delivery_time'] = (int)$value;
    }
}