<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 7/27/18
 * Time: 09:12
 */

namespace Inside\Core\Models;

use Illuminate\Database\Eloquent\Model;
class PaymentReceipt extends Model
{
    protected $table = 'payment_receipt';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'payment_receipt_code',
        'reference_code',
        'payment_method',
        'payment_amount',
        'payment_balance',
        'note',
        'customer_id',
        'company_id',
        'staff_id',
        'user_id',
        'store_id',
        'reconcile_id',
        'status',
        'created_at'
    ];
}