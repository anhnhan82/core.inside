<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 8/28/17
 * Time: 14:49
 */

namespace Inside\Core\Models;


class ShipperLocation extends BaseModel
{

    protected $fillable = [
        'user_id', // user_id with type = shipper
        'city_id',
        'district_id',
        'ward_id',
        'fixed',
        'ctime',
    ];

    public function setUserIdAttribute($value){
        $this->attributes['user_id'] = (int)$value;
    }
    public function setCityIdAttribute($value){
        $this->attributes['city_id'] = (int)$value;
    }
    public function setCustomerIdAttribute($value){
        $this->attributes['customer_id'] = (int)$value;
    }
    public function setDistrictIdAttribute($value){
        $this->attributes['district_id'] = (int)$value;
    }
    public function setWardIdAttribute($value){
        $this->attributes['ward_id'] = (int)$value;
    }
    public function setFixedAttribute($value){
        $this->attributes['fixed'] = (int)$value;
    }
    public function setCtimeAttribute($value){
        $this->attributes['ctime'] = (int)$value;
    }
}