<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 8/24/17
 * Time: 19:41
 */

namespace Inside\Core\Models;


class ShippingOrder extends BaseModel
{

    const STATUS_DELETED = 0;
    const STATUS_SHIPPING = 1;
    const STATUS_COMPLETED = 3;
    const STATUS_CANCEL = 5;
    const STATUS_RETURN = 7;
    const STATUS_RE_SHIPPING = 9;
    const STATUS_DELIVERY = 10;

    protected $fillable =[
        'id',
        'order_code',
        'packed_code',
        'customer_code',
        'customer_id',
        'address',
        'name',
        'phone',
        'district_id',
        'ward_id',
        'city_id',
        'cod',
        'fee',
        'amount',
        'weight', // khoi luong tu tinh
        'customer_weight', // khoi luong do toi tac tinh
        'note',
        'item_content',
        'shipper_id',
        'company_id',
        'status',
        'order_status',
        'created_at',
        'updated_at',
        'ctime',
        'utime',
        'mysql_id',
        'shipping_date', // hen ngay giao
        'total_item', // Total item in shipping order
        'properties', // Store additional information
        'priority',
        'expected_delivery_time',
        'receiving_time',
        'deliveried_time',
    ];


    public function setOrderCodeAttribute($value){
        $this->attributes['order_code'] = (int)$value;
    }
    public function setPackedCodeAttribute($value){
        $this->attributes['packed_code'] = (int)$value;
    }
    public function setSkuAttribute($value){
        $this->attributes['sku'] = (int)$value;
    }
    public function setCustomerIdAttribute($value){
        $this->attributes['customer_id'] = (int)$value;
    }
    public function setDistrictIdAttribute($value){
        $this->attributes['district_id'] = (int)$value;
    }
    public function setWardIdAttribute($value){
        $this->attributes['ward_id'] = (int)$value;
    }
    public function setCityIdAttribute($value){
        $this->attributes['city_id'] = (int)$value;
    }
    public function setCodAttribute($value){
        $this->attributes['cod'] = (int)$value;
    }
    public function setFeeAttribute($value){
        $this->attributes['fee'] = (int)$value;
    }
    public function setShipperIdAttribute($value){
        $this->attributes['shipper_id'] = (int)$value;
    }
    public function setCompanyIdAttribute($value){
        $this->attributes['company_id'] = (int)$value;
    }
    public function setStatusAttribute($value){
        $this->attributes['status'] = (int)$value;
    }
    public function setOrderStatusAttribute($value){
        $this->attributes['order_status'] = (int)$value;
    }
    public function setAmountAttribute($value){
        $this->attributes['amount'] = (int)$value;
    }
    public function setWeightAttribute($value){
        $this->attributes['weight'] = (double)$value;
    }
    public function setCustomerWeightAttribute($value){
        $this->attributes['customer_weight'] = (double)$value;
    }
    public function setCtimeAttribute($value){
        $this->attributes['ctime'] = (int)$value;
    }
    public function setUtimeAttribute($value){
        $this->attributes['utime'] = (int)$value;
    }
    public function setMysqlIdAttribute($value){
        $this->attributes['mysql_id'] = (int)$value;
    }
    public function setShippingDateAttribute($value){
        $this->attributes['shipping_date'] = (int)$value;
    }
    public function setTotalItemAttribute($value){
        $this->attributes['total_item'] = (int)$value;
    }
    public function setPriorityAttribute($value){
        $this->attributes['priority'] = (int)$value;
    }

    public function setPropertiesAttribute($value){
        if($value){
            if(is_array($value)){
                $this->attributes['properties'] = $value;
            }else{
                $this->attributes['properties'] = json_decode($value, true);
            }
        }else{
            $this->attributes['properties'] = [];
        }
    }
    public function setExpectedDeliveryTimeAttribute($value){
        $this->attributes['expected_delivery_time'] = (int)$value;
    }
    public function setReceivingTimeAttribute($value){
        $this->attributes['receiving_time'] = (int)$value;
    }
    public function setDeliveriedTimeAttribute($value){
        $this->attributes['deliveried_time'] = (int)$value;
    }



}