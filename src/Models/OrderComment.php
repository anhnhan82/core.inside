<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 9/14/17
 * Time: 16:09
 */

namespace Inside\Core\Models;


class OrderComment extends BaseModel
{
    protected $fillable = [
        'user_id',
        'order_code',
        'content',
        'mysql_id',
    ];

    public function setUserIdAttribute($value){
        $this->attributes['user_id'] = (int)$value;
    }
    public function setOrderCodeAttribute($value){
        $this->attributes['order_code'] = (int)$value;
    }
    public function setMysqlIdAttribute($value){
        $this->attributes['mysql_id'] = (int)$value;
    }
}