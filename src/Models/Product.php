<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/22/17
 * Time: 06:27
 */

namespace Inside\Core\Models;


class Product extends BaseModel
{
    const NORMAL = 1;
    const COMBO = 2;
    const MATERIAL = 3;

    protected $fillable = [
        'id',
        'store_id',
        'sku',
        'name',
        'barcode',
        'price',
        'latest_cost',
        'category_id',
        'brand_id',
        'status',
        'type',
        'mysql_id',
    ];

    public function setStoreIdAttribute($value){
        $this->attributes['store_id'] = (int)$value;
    }
    public function setSkuAttribute($value){
        $this->attributes['sku'] = (int)$value;
    }
    public function setBarcodeAttribute($value){
        $this->attributes['barcode'] = (int)$value;
    }
    public function setPriceAttribute($value){
        $this->attributes['price'] = (int)$value;
    }
    public function setLatestCostAttribute($value){
        $this->attributes['latest_cost'] = (int)$value;
    }
    public function setCategoryIdAttribute($value){
        $this->attributes['category_id'] = (int)$value;
    }
    public function setBrandIdAttribute($value){
        $this->attributes['brand_id'] = (int)$value;
    }
    public function setTypeAttribute($value){
        $this->attributes['type'] = (int)$value;
    }

    public function setMysqlIdAttribute($value){
        $this->attributes['mysql_id'] = (int)$value;
    }
}