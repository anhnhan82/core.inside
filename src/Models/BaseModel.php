<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/21/17
 * Time: 14:53
 */

namespace Inside\Core\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class BaseModel extends Eloquent
{
    const ACTIVE = 1;
    const DELETE = 0;
    protected $connection = 'core_database';
}