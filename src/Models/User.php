<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/24/17
 * Time: 21:51
 */

namespace Inside\Core\Models;


class User extends BaseModel
{
    protected $fillable = [
        'id',
        'name',
        'facebook_id',
        'email',
        'avatar',
        'role_id',
        'status',
        'mysql_id',
    ];

    protected $hidden = [
        'password',
    ];

    public function setRoleIdAttribute($value){
        $this->attributes['role_id'] = (int)$value;
    }
    public function setStatusAttribute($value){
        $this->attributes['status'] = (int)$value;
    }
    public function setMysqlIdAttribute($value){
        $this->attributes['mysql_id'] = (int)$value;
    }

}