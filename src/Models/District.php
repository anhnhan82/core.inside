<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/24/17
 * Time: 22:03
 */

namespace Inside\Core\Models;


class District extends BaseModel
{

    protected $fillable = [
        'id',
        'city_id',
        'name',
        'code',
        'status',
        'mysql_id',
    ];

    public function setStatusAttribute($value){
        $this->attributes['status'] = (int)$value;
    }

    public function setCityIdAttribute($value){
        $this->attributes['city_id'] = (int)$value;
    }

    public function setMysqlIdAttribute($value){
        $this->attributes['mysql_id'] = (int)$value;
    }
}