<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 7/6/18
 * Time: 10:57
 */

namespace Inside\Core\Models;
use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{

    protected $table = 'voucher';
    public $timestamps = false;
    protected $primaryKey = 'voucher_id';
    protected $connection = 'mysql';
    protected $fillable = [
        'voucher_type',
        'voucher_group_id',
        'voucher_code',
        'voucher_source_code',
        'voucher_used_code',
        'voucher_name',
        'voucher_desc',
        'voucher_percent',
        'voucher_price',
        'voucher_status',
        'voucher_customer_id',
        'voucher_exp_date',
        'voucher_user_id',
        'voucher_ctime',
        'voucher_utime',
        'voucher_thirdparty_code',
        'voucher_sku_service',
    ];
}