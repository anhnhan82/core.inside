<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 7/29/17
 * Time: 07:15
 */

namespace Inside\Core\Models;


class Address extends BaseModel
{
    protected $fillable = [
        'id',
        'name',
        'ward_id',
        'district_id',
        'city_id',
        'lon',
        'lat',
        'status',
    ];

    public function setStatusAttribute($value){
        $this->attributes['status'] = (int)$value;
    }
    public function setWardIdAttribute($value){
        $this->attributes['ward_id'] = (int)$value;
    }

    public function setDistrictIdAttribute($value){
        $this->attributes['district_id'] = (int)$value;
    }
    public function setCityIdAttribute($value){
        $this->attributes['city_id'] = (int)$value;
    }
    public function setLonAttribute($value){
        $this->attributes['lon'] = (double)$value;
    }
    public function setLatAttribute($value){
        $this->attributes['lat'] = (double)$value;
    }

}