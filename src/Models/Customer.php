<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/24/17
 * Time: 21:55
 */

namespace Inside\Core\Models;


class Customer extends BaseModel
{
    const TYPE_NORMAL = 1;

    protected $fillable = [
        'id',
        'name',
        'first_name',
        'last_name',
        'phone',
        'email',

        'city_id',
        'district_id',
        'ward_id',
        'address',
        'note',
        'facebook_url',
        'type',
        'balance',
        'promotion',
        'promotion_spa',
        'status',
        'sns_id',
        'mysql_id',
        'address_id',
    ];

    protected $hidden = [
        'password',
    ];

    public function setStatusAttribute($value){
        $this->attributes['status'] = (int)$value;
    }
    public function setWardIdAttribute($value){
        $this->attributes['ward_id'] = (int)$value;
    }

    public function setDistrictIdAttribute($value){
        $this->attributes['district_id'] = (int)$value;
    }
    public function setCityIdAttribute($value){
        $this->attributes['city_id'] = (int)$value;
    }
    public function setTypeAttribute($value){
        $this->attributes['type'] = (int)$value;
    }
    public function setBalanceAttribute($value){
        $this->attributes['balance'] = (int)$value;
    }
    public function setPromotionAttribute($value){
        $this->attributes['promotion'] = (int)$value;
    }
    public function setPromotionSpaAttribute($value){
        $this->attributes['promotion_spa'] = (int)$value;
    }
    public function setMysqlIdAttribute($value){
        $this->attributes['mysql_id'] = (int)$value;
    }
}