<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 7/29/17
 * Time: 07:04
 */

namespace Inside\Core\Models;


class City extends BaseModel
{
    protected $fillable = [
        'id',
        'name',
        'code',
        'status',
        'is_far',
        'mysql_id',
    ];

    public function setStatusAttribute($value){
        $this->attributes['status'] = (int)$value;
    }
    public function setIsFarAttribute($value){
        $this->attributes['is_far'] = (int)$value;
    }
    public function setMysqlIdAttribute($value){
        $this->attributes['mysql_id'] = (int)$value;
    }
}