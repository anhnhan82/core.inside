<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/24/17
 * Time: 22:04
 */

namespace Inside\Core\Models;


class Ward extends BaseModel
{
    protected $fillable = [
        'id',
        'city_id',
        'district_id',
        'name',
        'code',
        'status',
        'mysql_id',
        'is_far',
    ];
    public function setCustomerIdAttribute($value){
        $this->attributes['customer_id'] = (int)$value;
    }
    public function setStatusAttribute($value){
        $this->attributes['status'] = (int)$value;
    }
    public function setWardIdAttribute($value){
        $this->attributes['ward_id'] = (int)$value;
    }

    public function setDistrictIdAttribute($value){
        $this->attributes['district_id'] = (int)$value;
    }
    public function setCityIdAttribute($value){
        $this->attributes['city_id'] = (int)$value;
    }
    public function setIsFarAttribute($value){
        $this->attributes['is_far'] = (int)$value;
    }
    public function setMysqlIdAttribute($value){
        $this->attributes['mysql_id'] = (int)$value;
    }

}