<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/24/17
 * Time: 22:04
 */

namespace Inside\Core\Models;


class Notification extends BaseModel
{
    const TYPE_NORMAL = 1;
    const TYPE_FEEDBACK = 2;

    const STATUS_ACTIVE = 1;
    const STATUS_UN_READ = 2;
    const STATUS_DELETE = 4;
    const STATUS_READ = 8;
    const STATUS_FEEDBACK = 16;

    const APP_GO = 1;
    const APP_FOOD = 2;
    const APP_WEB = 4;

    protected $fillable = [
        'user_id',
        'type', // Normal, feedback
        'app', // Go, food, web inside
        'title',
        'message',
        'status',
        'options',
        'feedback_at',
    ];

    public function setUserIdAttribute($value){
        $this->attributes['user_id'] = (int)$value;
    }
    public function setTypeAttribute($value){
        $this->attributes['type'] = (int)$value;
    }
    public function setStatusAttribute($value){
        $this->attributes['status'] = (int)$value;
    }
    public function setAppAttribute($value){
        $this->attributes['app'] = (int)$value;
    }

    public function setOptionsAttribute($value){
        if(!$value){
            $this->attributes['options'] = [];
        }else{
            if(is_array($value)){
                $this->attributes['options'] = $value;
            }else{
                $this->attributes['options'] = json_decode($value, true);
            }

        }
    }
}