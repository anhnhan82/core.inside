<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/21/17
 * Time: 11:45
 */
return [
    'core_database' => [
        'driver'   => 'mongodb',
        'host'     => env('CORE_DB_HOST', 'localhost'),
        'port'     => env('CORE_DB_PORT', 27017),
        'database' => env('CORE_DB_DATABASE', 'logs'),
        'username' => env('CORE_DB_USERNAME'),
        'password' => env('CORE_DB_PASSWORD'),
        'options'  => [
            'database' => env('CORE_DB_DATABASE_AUTH', 'admin') // sets the authentication database required by mongo 3
        ]
    ],

];