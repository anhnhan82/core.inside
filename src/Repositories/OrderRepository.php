<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/21/17
 * Time: 14:57
 */

namespace Inside\Core\Repositories;

use Inside\Core\Models\Order;
use Inside\Core\Repositories\Contracts\OrderRepositoryInterface;

class OrderRepository extends BaseRepository implements OrderRepositoryInterface
{

    public function __construct(Order $model)
    {
        parent::__construct($model);
    }

    /**
     * Get summary of amount
     * @param array $condition
     * @return mixed
     */
    public function getTotalAmount(array $condition){
        $query = $this->model;
        if (count($condition)) {
            $query = $this->_buildCondition($query, $condition);
        }

        return $query->sum('total');
    }


}