<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/24/17
 * Time: 22:37
 */

namespace Inside\Core\Repositories;


use Inside\Core\Models\City;
use Inside\Core\Repositories\Contracts\CityRepositoryInterface;

class CityRepository extends BaseRepository implements CityRepositoryInterface
{

    public function __construct(City $model)
    {
        parent::__construct($model);
    }
}