<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 8/24/17
 * Time: 19:42
 */

namespace Inside\Core\Repositories;


use Inside\Core\Models\ShippingOrder;
use Inside\Core\Repositories\Contracts\ShippingOrderRepositoryInterface;
use DB;

class ShippingOrderRepository extends BaseRepository implements ShippingOrderRepositoryInterface
{
    public function __construct(ShippingOrder $model)
    {
        parent::__construct($model);
    }

    /**
     * Get total amount of shipping order
     * @param array $condition
     * @return mixed
     */
    public function getTotalAmount(array $condition){
        $query = $this->model;
        if (count($condition)) {
            $query = $this->_buildCondition($query, $condition);
        }

        return $query->sum('cod');
    }

}