<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 7/6/18
 * Time: 11:00
 */

namespace Inside\Core\Repositories;


use Illuminate\Support\Facades\DB;

use Inside\Core\Jobs\VoucherJob;
use Inside\Core\Models\PaymentReceipt;
use Inside\Core\Models\Voucher;

use Inside\Core\Repositories\Contracts\VoucherRepositoryInterface;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class VoucherRepository implements VoucherRepositoryInterface
{
    protected $model;
    protected $guidRepository;

    const VOUCHER_GIFT_CARD = 12;

    public function __construct(Voucher $model)
    {
        $this->model = $model;

    }

    protected function _getRedis()
    {
        $redis = new \Redis();
        $redis->connect(env('REDIS_HOST', '127.0.0.1'), env('REDIS_PORT', 6379));
        return $redis;
    }

    public function validate(array $arrCode)
    {
        $arrResult = [];
        $prefix = env('APP_ENV', 'local') . '::gift-card::code-';
        $objRedis = $this->_getRedis();
        foreach ($arrCode as $code) {
            $arrValid = ['valid' => 0, 'value' => 0, 'code' => $code, 'serial_number' => ''];
            $strData = $objRedis->get($prefix . $code);
            if ($strData) {
                $arrInfo = json_decode($strData, true);

                if ($arrInfo) {
                    $arrValid['valid'] = ($arrInfo['status'] == 1 ? 1 : 0);
                    $arrValid['value'] = $arrInfo['voucher_price'];
                    $arrValid['serial_number'] = $arrInfo['serial_number'];
                }
            }
            $arrResult[] = $arrValid;
        }

        return $arrResult;
    }

    public function redeem(array $arrParam)
    {
        $allValid = true;
        $arrCodes = explode(',', $arrParam['code']);
        $arrResults = $this->validate($arrCodes);
        foreach ($arrResults as $arrResult) {
            if ($arrResult['valid'] == false) {
                $allValid = false;
                break;
            }
        }

        if (!$allValid) {
            return ['error' => 1, 'redeem' => $arrResults, 'message' => 'Some of voucher code is not valid'];
        }

        try {
            /*
            // Mark gift code is used
            $arrCaches = [];
            $prefix = env('APP_ENV', 'local') . '::gift-card::code-';
            $objRedis = $this->_getRedis();
            $connection = new AMQPStreamConnection(env('RABBITMQ_HOST', 'localhost'), env('RABBITMQ_PORT', 5672), env('RABBITMQ_USER', 'guest'), env('RABBITMQ_PASSWORD', 'guest'), env('RABBITMQ_QUEUE', '/'));
            $channel = $connection->channel();
            foreach ($arrResults as $arrVoucher) {
                $arrVoucher['status'] = 2; // Used
                $arrVoucher['used_at'] = time();
                $objRedis->set($prefix . $arrVoucher['code'], json_encode([
                    'status' => 2,
                    'voucher_price' => $arrVoucher['value'],
                    'serial_number' => $arrVoucher['serial_number'],
                    'voucher_code' => $arrVoucher['code'],
                ]));
                $arrPushParam = [
                    'command' => 'Voucher@redeem',
                    'param' => [
                        'voucher_code' => $arrVoucher['code'],
                        'used_code' => $arrParam['used_code'],
                        'used_at' => time(),
                    ]
                ];
                $msg = new AMQPMessage(json_encode($arrPushParam));
                $channel->basic_publish($msg, '', 'voucher');
            }
            $channel->close();
            $connection->close();
            */

            // Mark gift code is used
            $arrCaches = [];
            $prefix = env('APP_ENV', 'local') . '::gift-card::code-';
            $objRedis = $this->_getRedis();

            foreach ($arrResults as $arrVoucher) {
                $arrVoucher['status'] = 2; // Used
                $arrVoucher['used_at'] = time();
                $objRedis->set($prefix . $arrVoucher['code'], json_encode([
                    'status' => 2,
                    'voucher_price' => $arrVoucher['value'],
                    'serial_number' => $arrVoucher['serial_number'],
                    'voucher_code' => $arrVoucher['code'],
                    'time' => time(),
                    'source' => 'web',
                ]));
            }

            return ['error' => 0, 'redeem' => $arrResults, 'message' => 'All voucher redeem'];
        } catch (\Exception $ex) {
            foreach ($arrCaches as $code => $arrCache) {
                $objRedis->set($prefix . $code, json_encode([
                    'status' => 1,
                    'voucher_price' => $arrCache['value'],
                    'serial_number' => $arrCache['serial_number'],
                    'voucher_code' => $arrCache['code'],
                    'time' => time(),
                    'source' => 'web_exception',
                ]));
            }
            return ['error' => 1, 'redeem' => $arrResults, 'message' => 'Unknown error'];
        }
    }
}