<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/24/17
 * Time: 22:32
 */

namespace Inside\Core\Repositories;

use Inside\Core\Models\Customer;
use Inside\Core\Repositories\Contracts\CustomerRepositoryInterface;

class CustomerRepository extends BaseRepository implements CustomerRepositoryInterface
{
    public function __construct(Customer $model)
    {
        parent::__construct($model);
    }

}