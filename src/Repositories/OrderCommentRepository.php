<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 9/14/17
 * Time: 16:14
 */

namespace Inside\Core\Repositories;

use Inside\Core\Models\OrderComment;
use Inside\Core\Repositories\Contracts\OrderCommentRepositoryInterface;

class OrderCommentRepository extends BaseRepository implements OrderCommentRepositoryInterface
{
    public function __construct(OrderComment $model)
    {
        parent::__construct($model);
    }
}