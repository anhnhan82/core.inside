<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/24/17
 * Time: 22:38
 */

namespace Inside\Core\Repositories;


use Inside\Core\Models\District;
use Inside\Core\Repositories\Contracts\DistrictRepositoryInterface;

class DistrictRepository extends BaseRepository implements DistrictRepositoryInterface
{

    public function __construct(District $model)
    {
        parent::__construct($model);
    }
}