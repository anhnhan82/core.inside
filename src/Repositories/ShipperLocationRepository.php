<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 8/28/17
 * Time: 15:12
 */

namespace Inside\Core\Repositories;

use Inside\Core\Models\ShipperLocation;
use Inside\Core\Repositories\Contracts\ShipperLocationRepositoryInterface;


class ShipperLocationRepository extends BaseRepository implements ShipperLocationRepositoryInterface
{

    public function __construct(ShipperLocation $model)
    {
        parent::__construct($model);
    }

    /**
     * Get list location can ship by user
     * @param array $arrParam
     * @return array
     */
    public function getListLocationByUser(array $arrParam){
        $arrOwnDist = [];
        $arrOwnWard = [];
        $arrNotWard = [];

        // Get list district and ward of current user
        $arrTmp = $this->model->where([
            'user_id' => $arrParam['user_id'],
        ])->get();
        if($arrTmp){
            foreach ($arrTmp as $arrLoc){
                if($arrLoc['ward_id'] > 0){
                    $arrOwnWard[] = $arrLoc['ward_id'];
                }
                if(!in_array($arrLoc['district_id'], $arrOwnDist)){
                    $arrOwnDist[] = $arrLoc['district_id'];
                }
            }
        }
        // Get list ward in district but not delivery by user
        $arrTmpNotIn = $this->model->where('user_id', '<>',  $arrParam['user_id'])
            ->whereIn('district_id', $arrOwnDist)
            ->get();
        if($arrTmpNotIn){
            foreach ($arrTmpNotIn as $arrLoc){
                if($arrLoc['ward_id'] > 0){
                    $arrNotWard[] = $arrLoc['ward_id'];
                }
            }
        }

        return [
            'district_id' => $arrOwnDist,
            'ward_id' => $arrOwnWard,
            'exclude_ward_id' => $arrNotWard,
        ];
    }
}