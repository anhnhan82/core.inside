<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/21/17
 * Time: 15:04
 */

namespace Inside\Core\Repositories;

use Inside\Core\Models\BaseModel;
use Inside\Core\Repositories\Contracts\BaseRepositoryInterface;

class BaseRepository implements BaseRepositoryInterface
{
    protected $model;

    public function __construct(BaseModel $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        // TODO: Implement all() method.
    }

    public function findById($id)
    {
        return $this->model->where('id', $id)->first();
    }

    public function create(array $params)
    {
        $model = $this->model->create($params);
        return $model;
    }

    public function update($id, array $params)
    {
        $model = $this->model->where('id', $id)->first();
        if(!$model){
            return null;
        }
        $model->fill($params)->save();

        return $model;
    }

    public function updateBy(array $condition, array $params)
    {
        if(!$condition){
            return false;
        }
        $model = $this->_buildCondition($this->model, $condition);
        $model->update($params);

        return $model;
    }

    /**
     * Update many model by condition
     * @param array $condition
     * @param array $params
     * @return mixed
     */
    public function updateManyBy(array $condition, array $params)
    {
        if(!$condition){
            return false;
        }
        $models = $this->_buildCondition($this->model, $condition)->get();
        if(!$models->count()){
            return false;
        }
        foreach ($models as $model){
            $model->fill($params)->save();
        }

        return true;
    }

    /**
     * Find model by condition
     * @param array $condition
     * @param null $fields
     * @param null $sort
     * @param int $limit
     * @param int $offset
     * @return mixed
     * Usage
     *   $condition = [];
     *   $condition[] = $this->createConditionField('user_id', $user_id, '=');
     *   $condition[] = $this->createConditionField('title', 'item', 'like');
     *   $model->find($condition, null, ['field' => 'score', 'dir' => 'desc']);
     */
    public function find(array $condition, $fields = null, $sort = null, $limit = -1, $offset = 0)
    {
        $query = $this->model;

        // Chẹck has condition
        if (count($condition)) {
            $query = $this->_buildCondition($query, $condition);
        }

        // Has field
        if (is_array($fields)) {
            $query->select($fields);
        }

        // Has sort
        if (is_array($sort)) {
            $field = $sort['field'];
            $dir = isset($sort['dir']) ? $sort['dir'] : 'desc';
            if(is_array($field)){
                foreach ($field as $fld){
                    $query->orderBy($fld, $dir);
                }
            }else{
                $query->orderBy($field, $dir);
            }
        }

        // Has limit
        if ($limit > 0) {
            $query->take($limit);
        }

        // Has offset
        if ($offset > 0) {
            $query->skip($offset);
        }

        // Execute query
        return $query->get();
    }

    public function findOne(array $condition, $fields = null)
    {
        $query = $this->model;

        // Chẹck has condition
        if (count($condition)) {
            $query = $this->_buildCondition($query, $condition);
        }

        // Has field
        if (is_array($fields)) {
            $query->select($fields);
        }

        // Execute query
        return $query->first();
    }

    /**
     * Count item match condition
     * @param array $condition
     * @return mixed
     */
    public function findCount(array $condition){
        $query = $this->model;
        // Chẹck has condition
        if (count($condition)) {
            $query = $this->_buildCondition($query, $condition);
        }

        return $query->count();
    }

    protected function _buildCondition($query, array $condition){
        if (count($condition)) {
            $query = $query->where(function ($qr) use ($condition) {
                foreach ($condition as $key => $cond) {
                    if (is_a($cond, 'stdClass')) {
                        switch (strtolower($cond->operator)){
                            case 'wherein':
                                $arrValue = is_array($cond->value) ? $cond->value : [$cond->value];
                                $qr->whereIn($cond->field, $arrValue);
                                break;
                            case 'wherenotin':
                                $arrValue = is_array($cond->value) ? $cond->value : [$cond->value];
                                $qr->whereNotIn($cond->field, $arrValue);
                                break;
                            case 'whereor':
                                $qr->Where(function ($qra)use ( $cond){
                                    $qra->orWhere(function ($oqr) use ( $cond){
                                        //return $this->_buildCondition($oqr, $cond->conditions);

                                        foreach ($cond->conditions as $con){
                                            $value = $con->operator == 'like' ? '%' . $con->value . '%' : $con->value;
                                            //$oqr->orwhere($con->field, $con->operator, $value);
                                            $oqr->orwhere($con->field, $con->operator, $value);
                                        }
                                    });
                                });
                                break;
                            default:
                                $value = $cond->operator == 'like' ? '%' . $cond->value . '%' : $cond->value;
                                $qr->where($cond->field, $cond->operator, $value);
                                break;
                        }

                    } else {
                        $qr->where($key, $cond);
                    }
                }
            });
        }
        return $query;
    }

    /**
     * Create condition for filter
     * @param $name
     * @param $value
     * @param string $operator like, >, >=, <, <=, =
     * @return \stdClass
     */
    public function createCondition($name, $value, $operator = '='){
        $cond = new \stdClass;
        $cond->field = $name;
        $cond->value = $value;
        $cond->operator = $operator;

        return $cond;
    }

    /**
     * Create or condition
     * @param array $arrCond
     * @return \stdClass
     * Usage:
     *  // Find customer by name or phone
     *  $arrCond[] = $this->customerRepository->createOrCondition([$this->customerRepository->createCondition('phone', $strSearch, 'like')]);
     *  $arrCond[] = $this->customerRepository->createOrCondition([$this->customerRepository->createCondition('name', $strSearch, 'like')]);
     *  $arrCus = $customerRep->find($arrCond)
     *
     *  // Find customer by name and phone
     *  $arrCondOr[] = $this->customerRepository->createCondition([$this->customerRepository->createCondition('phone', $strSearch, 'like')]);
     *  $arrCondOr[] = $this->customerRepository->createCondition([$this->customerRepository->createCondition('name', $strSearch, 'like')]);
     *  $arrCond[] = $this->customerRepository->createOrCondition($arrCondOr);
     *  $arrCus = $customerRep->find($arrCond)
     */
    public function createOrCondition(array $arrCond){
        $cond = new \stdClass;

        $cond->operator = 'whereor';
        $cond->conditions = $arrCond;

        return $cond;
    }
}

