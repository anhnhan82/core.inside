<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 7/29/17
 * Time: 07:20
 */

namespace Inside\Core\Repositories;


use Inside\Core\Models\Address;

use Inside\Core\Repositories\Contracts\AddressRepositoryInterface;

class AddressRepository extends BaseRepository implements AddressRepositoryInterface
{
    public function __construct(Address $model)
    {
        parent::__construct($model);
    }
}