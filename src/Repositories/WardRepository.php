<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/24/17
 * Time: 22:38
 */

namespace Inside\Core\Repositories;


use Inside\Core\Models\Ward;
use Inside\Core\Repositories\Contracts\WardRepositoryInterface;

class WardRepository extends BaseRepository implements WardRepositoryInterface
{

    public function __construct(Ward $model)
    {
        parent::__construct($model);
    }
}