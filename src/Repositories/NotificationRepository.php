<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/24/17
 * Time: 22:38
 */

namespace Inside\Core\Repositories;


use Carbon\Carbon;
use Inside\Core\Models\Notification;
use Inside\Core\Repositories\Contracts\NotificationRepositoryInterface;
use MongoDB\BSON\UTCDateTime;

class NotificationRepository extends BaseRepository implements NotificationRepositoryInterface
{
    public function __construct(Notification $model)
    {
        parent::__construct($model);
    }

    /** Mark notification is read
     * @param $id
     * @param array $arrParam
     * @return \Illuminate\Database\Eloquent\Model|\Inside\Core\Models\BaseModel|int|null
     */
    public function markRead($id, array $arrParam){
        $model = $this->model->where('_id', $id)->first();
        if(!$model){
            return null;
        }
        // Not owner
        if($model->code != $arrParam['staff_code']){
            return 1000;
        }
        // Already read
        if($model->status & Notification::STATUS_READ){
            return 1001;
        }
        // Update read time
        $model->feedback_at = new UTCDateTime(Carbon::now()->getTimestamp() * 1000);
        $model->status = ($model->status | Notification::STATUS_READ);
        $model->save();

        return $model;
    }

    /**
     * Feedback no message
     * @param $id
     * @param array $arrParam
     * @return \Illuminate\Database\Eloquent\Model|\Inside\Core\Models\BaseModel|int|null
     */
    public function feedback($id, array $arrParam){
        $model = $this->model->where('_id', $id)->first();
        if(!$model){
            return null;
        }
        // Not owner
        if($model->code != $arrParam['staff_code']){
            return 1000;
        }
        if($model->status & Notification::STATUS_FEEDBACK){
            return 1002;
        }
        $model->feedback_at = new UTCDateTime(Carbon::now()->getTimestamp() * 1000);
        $model->status = ($model->status | Notification::STATUS_FEEDBACK);
        $model->options = [
            'reason_id' => (int)$arrParam['reason_id'],
            'message' => trim($arrParam['message']),
        ];
        $model->save();

        return $model;
    }
}