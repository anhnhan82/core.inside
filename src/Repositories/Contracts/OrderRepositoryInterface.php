<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/21/17
 * Time: 14:56
 */

namespace Inside\Core\Repositories\Contracts;


interface OrderRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * Get summary of amount
     * @param array $condition
     * @return mixed
     */
    public function getTotalAmount(array $condition);
}