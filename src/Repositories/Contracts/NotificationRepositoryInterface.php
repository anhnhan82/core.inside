<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/24/17
 * Time: 22:38
 */

namespace Inside\Core\Repositories\Contracts;

interface NotificationRepositoryInterface extends BaseRepositoryInterface
{
    /** Mark notification is read
     * @param $id
     * @param array $arrParam
     * @return \Illuminate\Database\Eloquent\Model|\Inside\Core\Models\BaseModel|int|null
     */
    public function markRead($id, array $arrParam);

    public function feedback($id, array $arrParam);

}