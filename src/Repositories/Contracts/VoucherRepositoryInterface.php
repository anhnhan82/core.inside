<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 7/6/18
 * Time: 11:00
 */

namespace Inside\Core\Repositories\Contracts;


interface VoucherRepositoryInterface
{

    public function validate(array $arrCode);

    public function redeem(array $arrCode);
}