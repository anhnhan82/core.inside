<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 8/28/17
 * Time: 15:10
 */

namespace Inside\Core\Repositories\Contracts;


interface ShipperLocationRepositoryInterface extends BaseRepositoryInterface
{

    /**
     * Get list location can ship by user
     * @param User $user
     * @return mixed
     */
    public function getListLocationByUser(array $arrParam);
}