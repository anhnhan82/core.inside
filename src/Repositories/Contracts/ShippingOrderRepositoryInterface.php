<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 8/24/17
 * Time: 19:41
 */

namespace Inside\Core\Repositories\Contracts;


interface ShippingOrderRepositoryInterface extends BaseRepositoryInterface
{

    /**
     * Get total amount of shipping order
     * @param array $condition
     * @return mixed
     */
    public function getTotalAmount(array $condition);

}