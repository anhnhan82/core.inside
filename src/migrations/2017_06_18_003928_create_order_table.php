<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 100);
            $table->integer('customer_id');
            $table->integer('city_id')->default(0);
            $table->integer('district_id')->default(0);
            $table->integer('ward_id')->default(0);
            $table->string('address')->comment('Customer address');
            $table->integer('user_id')->comment('User create order')->comment('User id is web or user id at offline');
            $table->integer('shipper_id')->comment('Shipper devilery order')->default(0);
            $table->integer('shipping_from_date')->default(0);
            $table->integer('shipping_to_date')->default(0);
            $table->integer('shipping_company_id')->comment('1: Hasaki, 2: Post, 3: Shopee')->default(1);
            $table->string('note', 255)->comment('Order noted')->nullable();
            $table->tinyInteger('priority')->default(1);
            $table->integer('total_item');
            $table->integer('sub_total');
            $table->integer('discount')->default(0);
            $table->integer('total');
            $table->string('voucher_code', 50)->nullable();
            $table->integer('shipping_fee')->default(0);
            $table->integer('payment_amount')->default(0);
            $table->tinyInteger('payment_method')->default(0);
            $table->tinyInteger('status')->comment('1: pending, 2: processing, 4: shipped, 6: completed, 8: cancel, 10: return, 12: re-processing, 14: delivered, 16: packed')->default(1);
            $table->integer('printed_date')->comment('Date which order printed')->default(0);
            $table->integer('completed_date')->default(0);
            $table->tinyInteger('is_check')->default(0);
            $table->integer('store_id')->comment('Web or offline')->default(0);
            $table->mediumText('options')->nullable();

            $table->integer('billing_address_id')->index()->default(0);
            $table->integer('shipping_address_id')->index()->default(0);

            $table->timestamps();

            $table->index(['code', 'customer_id', 'user_id', 'shipper_id', 'status']);
            $table->index(['store_id', 'is_check', 'priority']);
            $table->index(['province_id', 'district_id', 'ward_id']);
            $table->index(['shipping_company_id', 'shipper_id', 'shipping_from_date', 'shipping_to_date']);
            $table->index(['printed_date', 'completed_date', 'created_at', 'updated_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
