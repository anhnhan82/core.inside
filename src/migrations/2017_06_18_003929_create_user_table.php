<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('facebook_id', 128);
            $table->string('email');
            $table->string('avatar', 128);
            $table->string('password');
            $table->rememberToken();

            $table->integer('role_id');
            $table->tinyInteger('status');

            $table->timestamps();

            $table->index(['id', 'email', 'status', 'role_id']);
            $table->index(['created_at', 'updated_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
