<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('first_name', 20);
            $table->string('last_name', 20);
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('password', 128)->nullable();
            $table->integer('city_id')->default(0);
            $table->integer('district_id')->default(0);
            $table->integer('ward_id')->default(0);
            $table->string('address', 255)->comment('Customer address')->nullable();
            $table->string('note', 255)->comment('Customer noted')->nullable();
            $table->string('facebook_url', 255)->comment('Facebook home')->nullable();
            $table->tinyInteger('type')->comment('1: Register by phone or email, 2: Facebook, 3: Google, 4: Yahoo, 5: Twitter')->default(1);
            $table->integer('balance')->default(0);
            $table->integer('promotion')->default(0);
            $table->integer('promotion_spa')->default(0);
            $table->tinyInteger('status')->default(1)->comment('1: active, 0: in-active');
            $table->string('sns_id')->comment('Id of ssn')->nullable();

            $table->integer('address_id')->default(0);
            $table->timestamps();

            $table->index(['id', 'phone', 'email', 'sns_id', 'sns_type', 'status']);
            $table->index(['province_id', 'district_id', 'ward_id', 'address_id']);

            $table->index(['address', 'promotion', 'created_at', 'updated_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
