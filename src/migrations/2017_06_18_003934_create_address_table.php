<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->integer('ward_id')->index();
            $table->integer('district_id')->index();
            $table->integer('city_id')->index();
            $table->decimal('lon', 10, 7)->nullable()->index();
            $table->decimal('lat', 10, 7)->nullable()->index();
            $table->tinyInteger('status')->commnent('1: Active, 0: delete, 2: Bad address')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
