<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_id');
            $table->integer('sku');
            $table->string('name', 200);
            $table->string('barcode', 100);
            $table->integer('price');
            $table->integer('latest_cost')->default(0);
            $table->integer('category_id')->default(0);
            $table->integer('brand_id')->default(0);
            $table->tinyInteger('status')->default(1)->comment('1: Active, 0: Delete');
            $table->tinyInteger('type')->default(1)->comment('1: Normal product, 2: Combo, 3: Material');
            $table->integer('mysql_id');

            $table->timestamps();

            $table->index(['id', 'store_id', 'sku', 'price', 'latest_cost', 'mysql_id']);
            $table->index(['created_at', 'updated_at', 'category_id', 'brand_id', 'status']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
