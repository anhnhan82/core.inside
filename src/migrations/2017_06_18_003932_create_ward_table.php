<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id');
            $table->integer('district_id');
            $table->string('name', 100);
            $table->string('code', 128);
            $table->tinyInteger('status')->default(1)->comment('1: Active, 0: Delete');

            $table->timestamps();

            $table->index(['id', 'city_id', 'district_id', 'name', 'code']);
            $table->index(['created_at', 'updated_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
