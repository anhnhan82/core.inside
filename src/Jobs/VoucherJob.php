<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 7/27/18
 * Time: 13:13
 */

namespace Inside\Core\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class VoucherJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $arrParams;

    public function __construct($arrParams)
    {
        $this->arrParams = $arrParams;
    }

    /**
     * Execute the job.
     */
    public function handle(){

    }
}