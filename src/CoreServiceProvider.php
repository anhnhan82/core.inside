<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/21/17
 * Time: 11:43
 */

namespace Inside\Core;

use Illuminate\Support\ServiceProvider;
use DB;
class CoreServiceProvider extends ServiceProvider
{
    protected $defer = false;

    public function boot()
    {
        $this->handleConfigs();
        $this->publishes([
            __DIR__ . '/migrations' => $this->app->databasePath() . '/migrations'
        ], 'migrations');

        if ($this->app->runningInConsole()) {
            // Add command here
            $this->commands([

            ]);
        }
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register(){
        $arrRepoName = array(
            'AddressRepository',
            'CityRepository',
            'CustomerRepository',
            'DistrictRepository',
            'GuidRepository',
            'OrderRepository',
            'ProductRepository',
            'UserRepository',
            'WardRepository',
            'ShippingOrderRepository',
            'ShipperLocationRepository',
            'OrderCommentRepository',
            'VoucherRepository',
        );

        foreach ($arrRepoName as $repoName) {
            $this->app->bind("Inside\\Core\\Repositories\\Contracts\\{$repoName}Interface", "Inside\\Core\\Repositories\\{$repoName}");
        }
    }

    private function handleConfigs()
    {
        /*
        $configPath = __DIR__ . '/Config/core.php';
        $this->publishes([$configPath => config_path('core.php')]);
        $this->mergeConfigFrom($configPath, 'core');
        $config = config('database.connections');
        dd($config, $configPath);
        */

        $configPath = __DIR__ . '/Config/core.php';
        $this->publishes([$configPath => config_path('core.php')]);
        $this->mergeConfigFrom($configPath, 'database.connections');

        if(env('CORE_LOG_QUERY', false) == true){
            DB::connection('core_database')->enableQueryLog();
        }
    }

}